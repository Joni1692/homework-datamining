import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
from sklearn import datasets
import random

def confusion(t,r):
	confusionMatrix = array([[0,0,0],[0,0,0],[0,0,0]])
	uniqueClasses = np.unique(t)
	numberofClasses = np.unique(t)
	numberofKlusters = r.shape[1]

	for i in range(0,numberofKlusters):
		clusterIndexes = np.where(r[:,i]==1)
		sumA = 0
		sumB = 0
		sumC = 0
		for j in clusterIndexes[0]:
			if j<50 :
				sumA = sumA + 1
			elif j<100 :
				sumB = sumB + 1
			else:
				sumC = sumC + 1
		if max(sumA,sumB,sumC) == sumA:
			confusionMatrix[0][0] = sumA + confusionMatrix[0][0]
			confusionMatrix[0][1] = sumB + confusionMatrix[0][1]
			confusionMatrix[0][2] = sumC + confusionMatrix[0][2]
 		if max(sumA,sumB,sumC) == sumB:
			confusionMatrix[1][1] = sumB + confusionMatrix[1][1]
			confusionMatrix[1][0] = sumA + confusionMatrix[1][0]
			confusionMatrix[1][2] = sumC + confusionMatrix[1][2]
		if max(sumA,sumB,sumC) == sumC:
			confusionMatrix[2][2] = sumC + confusionMatrix[2][2]
			confusionMatrix[2][0] = sumA + confusionMatrix[2][0]
			confusionMatrix[2][1] = sumB + confusionMatrix[2][1] 
	
	return confusionMatrix


def kMean(x,k,iteration):

	idx = random.sample(range(0,x.shape[0]),k)
	prototypes = x[idx,:]
	jValues = np.zeros(iteration)
	
	# for each iteration
	for i in range(0,iteration):
		distanceMatrix = array([np.zeros(k)])
		responsibilty = np.zeros((x.shape[0],k))
		# for each data point
		for row in x:
			distanceRow = []
			# for each prototype
			for pr in prototypes:
				sumSquare = 0
				# calculate the distance from datapoint to prototype
				for xValue, prValue in zip(row,pr):
					sumSquare = math.pow(xValue-prValue,2) + sumSquare
				
				distanceRow.append(sumSquare)
			
			distanceMatrix = np.append(distanceMatrix, [distanceRow], axis = 0)
		distanceMatrix = np.delete(distanceMatrix,0,0)
		
		# perform E-step (fill responisibilities tables)

		for j,r in zip(distanceMatrix,responsibilty):
			minIndex = np.argmin(j)
			for index in range(0,k):
				if index == minIndex:
					r[minIndex] = 1.0
				else:
					r[index] = 0.0



		# perform M-step (recompute prototypes)
		prototypeIndex = 0
		for pro in prototypes:

			# calculate sum of features for each cluster
			sumX = array(np.zeros(x.shape[1]))
			count = 0
			for l,j in zip(x,responsibilty):
				if(j[prototypeIndex] == 1):
					sumX = sumX + l
					count = count + 1

			# calculate new prototype
			for l in range(0,x.shape[1]):
				pro[l] = sumX[l] / count


			# pass to next prototype
			prototypeIndex = prototypeIndex + 1

		# compute J
		rowIndex = 0
		for u in responsibilty:
			columnIndex = 0
			for q in u:
				if q == 1.0:
					for xvalue,prvalue in zip(x[rowIndex],prototypes[columnIndex]):
						jValues[i] = jValues[i] + math.pow(xvalue-prvalue,2)
					break
				columnIndex = columnIndex + 1
			rowIndex = rowIndex + 1

	
	 # plot J 
	plt.scatter(range(1,iteration+1),jValues,color='red')
	xPlot, = plt.plot(range(1,iteration+1),jValues,color='red', label = 'J Values')

	plt.legend(loc='upper left')
	plt.show()
	return prototypes, responsibilty

# covariance matrix
cov1 = matrix([[1, 0.6],
           [0.6, 1.1]])

cov2 = matrix([[0.3, -0.2],
           [-0.2, 1]])
# mean vector
mean1 = array([-1, 1])
mean2 = array([1, -1])

x1, y1 = np.random.multivariate_normal(mean1, cov1, 75).T
x2, y2 = np.random.multivariate_normal(mean2, cov2, 150).T

finalMatrix = array([[0,0]])
for i,j in zip(x1,y1):
	finalMatrix = np.append(finalMatrix,[[i,j]],axis=0)

for i,j in zip(x2,y2):
	finalMatrix = np.append(finalMatrix,[[i,j]],axis=0)

finalMatrix = np.delete(finalMatrix,0,0)

pr1, r1 = kMean(finalMatrix,2,50)
pr2, r2 = kMean(finalMatrix,3,50)
pr3, r3 = kMean(finalMatrix,4,50)
pr4, r4 = kMean(finalMatrix,5,50)

firstCluster = r1[np.where(r1[:,0]==1)]
secondCluster = r1[np.where(r1[:,1]==1)]
# print firstCluster.shape[0]
# print secondCluster.shape[0]
# print

plt.bar([1,2],[firstCluster.shape[0],secondCluster.shape[0]])
plt.show()

firstCluster = r2[np.where(r2[:,0]==1)]
secondCluster = r2[np.where(r2[:,1]==1)]
thirdCluster = r2[np.where(r2[:,2]==1)]
# print firstCluster.shape[0]
# print secondCluster.shape[0]
# print thirdCluster.shape[0]
# print

plt.bar([1,2,3],[firstCluster.shape[0],secondCluster.shape[0],thirdCluster.shape[0]])
plt.show()

firstCluster = r3[np.where(r3[:,0]==1)]
secondCluster = r3[np.where(r3[:,1]==1)]
thirdCluster = r3[np.where(r3[:,2]==1)]
fourthCluster = r3[np.where(r3[:,3]==1)]
# print firstCluster.shape[0]
# print secondCluster.shape[0]
# print thirdCluster.shape[0]
# print fourthCluster.shape[0]
# print

plt.bar([1,2,3,4],[firstCluster.shape[0],secondCluster.shape[0],thirdCluster.shape[0],fourthCluster.shape[0]])
plt.show()

firstCluster = r4[np.where(r4[:,0]==1)]
secondCluster = r4[np.where(r4[:,1]==1)]
thirdCluster = r4[np.where(r4[:,2]==1)]
fourthCluster = r4[np.where(r4[:,3]==1)]
fivthCluster = r4[np.where(r4[:,4]==1)]
# print firstCluster.shape[0]
# print secondCluster.shape[0]
# print thirdCluster.shape[0]
# print fourthCluster.shape[0]
# print fivthCluster.shape[0]
# print

plt.bar([1,2,3,4,5],[firstCluster.shape[0],secondCluster.shape[0],thirdCluster.shape[0],fourthCluster.shape[0],fivthCluster.shape[0]])
plt.show()

iris = datasets.load_iris()

for i in range(3,12):
	pr5, r5 = kMean(iris.data,i,50)
	con = confusion(iris.target,r5)
	print con
	print 'Accuracy is: %f' %((con[0][0]+con[1][1]+con[2][2])/150.0*100)
	print 'Misclassification rate is: %f' %((con[0][1]+con[0][2]+con[1][0]+con[1][2]+con[2][0]+con[2][1])/150.0*100)



