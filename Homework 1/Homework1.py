import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
# covariance matrix
cov = matrix([[1, 0.2, -0.3],
           [0.2, 1.3, 0],
           [-0.3, 0, 8]])
# mean vector
mean = array([1,0,-1])

x, y, z = np.random.multivariate_normal(mean, cov, 100).T

# print x
# print y
# print z

# calculate batch estimate of mean
xSum = 0
ySum = 0
zSum = 0
for i, j, l in zip(x,y,z):
  xSum = xSum + i
  ySum = ySum + j
  zSum = zSum + l

print 'Batch estimate of mean:'
print matrix([(1.0/100)*xSum,(1.0/100)*ySum,(1.0/100)*zSum])
print  

# calculate sequential estimate of mean with initial mean [0,0,0]
mean = array([0,0,0])
meanArray = array([[0.0,0.0,0.0]])
counter = 1.0

xMean = 0.0
yMean = 0.0
zMean = 0.0

for i,j,l in zip(x,y,z):
  xMean = xMean + ((1.0/counter) * (i - xMean))
  yMean = yMean + ((1.0/counter) * (j - yMean))
  zMean = zMean + ((1.0/counter) * (l - zMean))
  meanArray = np.append(meanArray,[[xMean,yMean,zMean]],axis = 0)
  counter = counter + 1

meanArray = np.delete(meanArray, 0, 0)
print 'Sequential estimate of mean:'
print meanArray

plt.scatter(range(1,101),meanArray[:,0],color='red')
xPlot, = plt.plot(range(1,101),meanArray[:,0],color='red', label = 'xMean')

plt.scatter(range(1,101),meanArray[:,1],color='blue')
yPlot, = plt.plot(range(1,101),meanArray[:,1],color='blue', label = 'yMean')

plt.scatter(range(1,101),meanArray[:,2],color='green')
zPlot, = plt.plot(range(1,101),meanArray[:,2],color='green', label = 'zMean')

plt.legend(loc='upper left')
plt.show()
