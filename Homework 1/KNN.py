import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
from sklearn import datasets
import random

def runKNN(x,X,t,K):
	distanceMatrix = array([[0]])
	# For every element in training set
	for i in X:
		# initiate sumsquares 0
		sumSquares = 0
		# for every input, calulate the power of 2 of the difference between input and training data
		for j,l in zip(x,i):

			sumSquares = math.pow((j-l),2) + sumSquares
		# add the distance to the matrix
		distanceMatrix = np.append(distanceMatrix,[[math.sqrt(sumSquares)]],axis=1)

	distanceMatrix = np.delete(distanceMatrix,0,1)
	# get indexes of closest K neighbours
	indexes = distanceMatrix[0].argsort()[:K]
	# make prediction
	tvalues = t[indexes]
	unique, counts = np.unique(tvalues, return_counts=True)
	return unique[np.argmax(counts)]
	

# covariance matrix
cov1 = matrix([[1.0, 0.5],
           [0.5, 1.0]])
# mean vector
mean1 = array([1.0,1.0])

# covariance matrix 2 
cov2 = matrix([[1.0, -0.5],
           [-0.5, 1.0]])
# mean vector 2
mean2 = array([-1.0,-1.0])

x, y = np.random.multivariate_normal(mean1, cov1, 100).T
x2, y2 = np.random.multivariate_normal(mean2, cov2, 100).T


t=np.zeros((100))
t=np.append(t,np.ones(100),axis=0)

finalMatrix = array([[0,0]])
for i,j in zip(x,y):
	finalMatrix = np.append(finalMatrix,[[i,j]],axis=0)

for i,j in zip(x2,y2):
	finalMatrix = np.append(finalMatrix,[[i,j]],axis=0)

finalMatrix = np.delete(finalMatrix,0,0)

xTest,yTest = np.random.multivariate_normal(mean1, cov1, 200).T
xTest1,yTest1 = np.random.multivariate_normal(mean2, cov2, 200).T

# generate testing data
finalTest = array([[0,0]])

for i,j in zip(xTest,yTest):
	finalTest = np.append(finalTest,[[i,j]],axis=0)

for i,j in zip(xTest1,yTest1):
	finalTest = np.append(finalTest,[[i,j]],axis=0)

finalTest = np.delete(finalTest,0,0)

tTest=np.zeros((200))
tTest=np.append(tTest,np.ones(200),axis=0)

# print runKNN(finalTest[200],finalMatrix,t,5)
plotting = array([[0,0]])
for p in range(3,int(math.ceil(math.sqrt(400)))):
	counter = 0
	correct = 0

	for i in finalTest:
		output = runKNN(i,finalMatrix,t,p)
		if output == tTest[counter]:
			correct = correct + 1
		counter = counter + 1

	plotting = np.append(plotting, [[p,(correct/400.0)*100.0]],axis = 0)

plotting = np.delete(plotting,0,0)
plt.plot(plotting[:,0],plotting[:,1])
plt.show()
# Apply it to iris
iris = datasets.load_iris()

# Split into 100 for training, 50 for testing
idx = random.sample(range(0,150),50)
training = iris.data[idx,:]
trainingTargets = iris.target[idx]
testing = array([[0,0,0,0]])
targetTesting = []
for i in range(0,150):
	if i not in idx:
		testing = np.append(testing,[iris.data[i]],0)
		targetTesting.append(iris.target[i])
testing = np.delete(testing,0,0)

plotting = array([[0,0]])

for p in range(3,int(math.ceil(math.sqrt(iris.data.shape[0])))):
	correct = 0
	for i in range(0,100):
		output = runKNN(testing[i],training,trainingTargets,p)
		if output == targetTesting[i]:
			correct = correct + 1
	plotting = np.append(plotting, [[p,correct/100.0*100.0]],axis = 0)

plotting = np.delete(plotting,0,0)
plt.plot(plotting[:,0],plotting[:,1])
plt.show()