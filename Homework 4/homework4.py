import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
from sklearn import datasets
import random

#Sigmoid Function
def sigmoid (x):
	return 1/(1 + np.exp(-x))

#Derivative of Sigmoid Function
def derivatives_sigmoid(x):
	return x * (1 - x)

def forward_propagation(x,Wh,Wo):
	biasHiddenWeights = Wh[-1]
	biasOutputWeight = Wo[-1]
	weightHidden = np.delete(Wh, -1, axis=0)
	weightOutput = np.delete(Wo, -1, axis=0)
	
	# Calculate the input of the hidden layer
	noBiasHInput = np.dot(x,weightHidden)
	HInput = noBiasHInput + biasHiddenWeights

	# Calculate the output of the hidden layer
	HOutput = sigmoid(HInput)

	# Calculate the output of the output layer
	OInput = np.dot(HOutput,weightOutput)
	OInput = OInput + biasOutputWeight
	OOutput = sigmoid(OInput)

	return HInput, HOutput, OOutput

def back_propagation(x,t,Wh,Wo):
	HInput, HOutput, Output = forward_propagation(x,Wh,Wo)
	biasHiddenWeights = Wh[-1]
	biasOutputWeight = Wo[-1]
	weightHidden = np.delete(Wh, -1, axis=0)
	weightOutput = np.delete(Wo, -1, axis=0)

	learning_rate = 0.1

	# Calculate gradient error at output layer
	Error = t - Output
	
	# Compute slopes at output and hidden layers
	slopeOutput = derivatives_sigmoid(Output)
	slopeHidden = derivatives_sigmoid(HOutput)

	# Compute delta at output
	deltaOutput = Error * slopeOutput
	
	# Compute error at hidden layer
	errorHidden = deltaOutput.dot(weightOutput.T)

	# Compute delta at hidden layer
	deltaHidden = errorHidden * slopeHidden

	# Update weights at output and hiddenlayers
	weightOutput = weightOutput + HOutput.T.dot(deltaOutput) * learning_rate
	weightHidden = weightHidden + x.T.dot(HInput) * learning_rate

	# Update the biases at output and hiddenlayers
	biasOutputWeight = biasOutputWeight + np.sum(deltaOutput,axis = 0,keepdims=True) * learning_rate
	biasHiddenWeights = biasHiddenWeights + np.sum(deltaHidden,axis = 0,keepdims=True) * learning_rate

	return weightOutput,weightHidden

def NNtrain(x,t,Wh,Wo,iterations,learning_rate):
	# Separate biases from weights matrix
	biasHiddenWeights = Wh[-1]
	biasOutputWeight = Wo[-1]
	weightHidden = np.delete(Wh, -1, axis=0)
	weightOutput = np.delete(Wo, -1, axis=0)
	for i in range(0,iterations):
	
		# Calculate the input of the hidden layer
		noBiasHInput = np.dot(x,weightHidden)
		HInput = noBiasHInput + biasHiddenWeights

		# Calculate the output of the hidden layer
		HOutput = sigmoid(HInput)

		# Calculate the output of the output layer
		OInput = np.dot(HOutput,weightOutput)
		OInput = OInput + biasOutputWeight
		Output = sigmoid(OInput)

		# Calculate gradient error at output layer
		Error = t - Output
		
		# Compute slopes at output and hidden layers
		slopeOutput = derivatives_sigmoid(Output)
		slopeHidden = derivatives_sigmoid(HOutput)

		# Compute delta at output
		deltaOutput = Error * slopeOutput
		
		# Compute error at hidden layer
		errorHidden = deltaOutput.dot(weightOutput.T)

		# Compute delta at hidden layer
		deltaHidden = errorHidden * slopeHidden

		# Update weights at output and hiddenlayers
		weightOutput = weightOutput + HOutput.T.dot(deltaOutput) * learning_rate
		weightHidden = weightHidden + x.T.dot(HInput) * learning_rate

		# Update the biases at output and hiddenlayers
		biasOutputWeight = biasOutputWeight + np.sum(deltaOutput,axis = 0,keepdims=True) * learning_rate
		biasHiddenWeights = biasHiddenWeights + np.sum(deltaHidden,axis = 0,keepdims=True) * learning_rate

	weightHidden = np.append(weightHidden,biasHiddenWeights,axis=0)
	weightOutput = np.append(weightOutput,biasOutputWeight,axis=0)
	return weightHidden,weightOutput,Output





#First part, trying the algorithms for the forward and back propagation
x = np.array([[1,1]])

Wh = np.array([[-0.1, 0.2], [0.1,0.2],[0.1,-0.2]])
Wo = np.array([[0.1],[0.2],[0.1]]) 

a,b,c = forward_propagation(x,Wh,Wo)

print 'Input value of Hidden Layer: '
print a
print 'Output value of Hidden Layer:'
print b
print 'Output value of Neural Network:'
print c

d,e = back_propagation(x,np.array([[0]]),Wh,Wo)
# print d
# print e

# Second part, training and testing the NN

# covariance matrix
cov1 = matrix([[1, 0.5],
           [0.5, 1]])

cov2 = matrix([[1, -0.5],
           [-0.5, 1]])
# mean vector
mean1 = array([1, 1])
mean2 = array([-1, -1])

# Generate the training and testing sets

xTrain1, yTrain1 = np.random.multivariate_normal(mean1, cov1, 100).T
xTrain2, yTrain2 = np.random.multivariate_normal(mean2, cov2, 100).T

finalTrain = array([[0,0]])
for i,j in zip(xTrain1,yTrain1):
	finalTrain = np.append(finalTrain,[[i,j]],axis=0)

for i,j in zip(xTrain2,yTrain2):
	finalTrain = np.append(finalTrain,[[i,j]],axis=0)

finalTrain = np.delete(finalTrain,0,0)

xTest1, yTest1 = np.random.multivariate_normal(mean1, cov1, 1000).T
xTest2, yTest2 = np.random.multivariate_normal(mean2, cov2, 1000).T

finalTest = array([[0,0]])
for i,j in zip(xTest1,yTest1):
	finalTest = np.append(finalTest,[[i,j]],axis=0)

for i,j in zip(xTest2,yTest2):
	finalTest = np.append(finalTest,[[i,j]],axis=0)

finalTest = np.delete(finalTest,0,0)

# Create the targets
train = array([np.zeros(1)])
for i in range(0,99):
	train = np.append(train,[[0]],axis = 0)
for i in range(0,100):
	train = np.append(train,[[1]],axis = 0)

test = array([np.zeros(1)])
for i in range(0,999):
	test = np.append(test,[[0]],axis = 0)
for i in range(0,1000):
	test = np.append(test,[[1]],axis = 0)

# Generate random initial weights and biases

Wi1 = np.random.uniform(size=(finalTrain.shape[1] + 1,3))
Wi2 = np.random.uniform(size=(4,1))

# Train data
WH, WO, O = NNtrain(finalTrain,train,Wi1,Wi2,20,0.1)

# Find train data error
confusion = array([[0,0],[0,0]])
for i in range(0,200):
	if i<100:
		if round(O[i]) == 1:
			confusion[0][1] = confusion[0][1] + 1
		else:
			confusion[0][0] = confusion[0][0] + 1
	else:
		if round(O[i]) == 1:
			confusion[1][1] = confusion[1][1] + 1
		else:
			confusion[1][0] = confusion[1][0] + 1

print 'Confusion matrix for the training set:'
print confusion

# Predict testing set
HIn,HOut,Out = forward_propagation(finalTest,WH,WO)
# Find test data error
confusion = array([[0,0],[0,0]])
for i in range(0,2000):
	if i<1000:
		if round(Out[i]) == 1:
			confusion[0][1] = confusion[0][1] + 1
		else:
			confusion[0][0] = confusion[0][0] + 1
	else:
		if round(Out[i]) == 1:
			confusion[1][1] = confusion[1][1] + 1
		else:
			confusion[1][0] = confusion[1][0] + 1

print 'Confusion matrix for the testing set:'
print confusion