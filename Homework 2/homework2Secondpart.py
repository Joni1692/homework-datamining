import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
from sklearn import datasets
import random

# Load iris
iris = datasets.load_iris()

# Scatter two dimensions
plt.figure(figsize=(6, 4))
plt.scatter(iris.data[:,0],iris.data[:,1])
plt.ylabel('Sepal length')
plt.xlabel('Sepal width')
plt.show()

X = iris.data
# Calculate the means and covariance of the dataset
mean = np.mean(X,axis = 0)
covariance = (X - mean).T.dot((X - mean)) / (X.shape[0] - 1)
print 'Covariance matrix:'
print covariance
print 'Mean:'
print mean

# Calculate the eigenvectors and eigenvalues
eigenValues, eigenVectors = np.linalg.eig(covariance)

print 'EigenValues:'
print eigenValues
print 'EigenVectors'
print eigenVectors

# Make a list of (eigenvalue, eigenvector) tuples
eig_pairs = [(np.abs(eigenValues[i]), eigenVectors[:,i]) for i in range(len(eigenValues))]

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs.sort(key=lambda x: x[0], reverse=True)

# Calculate the explained variance for each component
total = sum(eigenValues)
explained_variance = [(i / total)*100 for i in sorted(eigenValues, reverse=True)]

# Plot the explained variance for each component
plt.figure(figsize=(6, 4))
plt.bar(range(len(explained_variance)), explained_variance, alpha=0.5, align='center',label='Explained variance')
plt.ylabel('Explained variance ratio')
plt.xlabel('Principal components')
plt.legend(loc='best')
plt.show()

# Construct the projection matrix, basically a matrix of our top concatenated vectors
matrix_w = np.hstack((eig_pairs[0][1].reshape(4,1),
                      eig_pairs[1][1].reshape(4,1)))

# Transform our samples into the new projection matrix
Y = X.dot(matrix_w)


# Plot the top two principal components

plt.figure(figsize=(6, 4))
plt.scatter(Y[:,0],Y[:,1])
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 2')
plt.legend(loc='lower center')
plt.show()

# Two other datasets
# First one is the boston house price dataset

boston = datasets.load_boston()

# Scatter two dimensions
plt.figure(figsize=(6, 4))
plt.scatter(boston.data[:,5],boston.data[:,6])
plt.ylabel('Rooms for dwelling')
plt.xlabel('Age')
plt.show()

X = boston.data

# Calculate the means and covariance of the dataset
mean = np.mean(X,axis = 0)
covariance = (X - mean).T.dot((X - mean)) / (X.shape[0] - 1)
print 'Covariance matrix:'
print covariance
print 'Mean:'
print mean

# Calculate the eigenvectors and eigenvalues
eigenValues, eigenVectors = np.linalg.eig(covariance)

print 'EigenValues:'
print eigenValues
print 'EigenVectors'
print eigenVectors

# Make a list of (eigenvalue, eigenvector) tuples
eig_pairs = [(np.abs(eigenValues[i]), eigenVectors[:,i]) for i in range(len(eigenValues))]

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs.sort(key=lambda x: x[0], reverse=True)

# Calculate the explained variance for each component
total = sum(eigenValues)
explained_variance = [(i / total)*100 for i in sorted(eigenValues, reverse=True)]

# Plot the explained variance for each component
plt.figure(figsize=(6, 4))
plt.bar(range(len(explained_variance)), explained_variance, alpha=0.5, align='center',label='Explained variance')
plt.ylabel('Explained variance ratio')
plt.xlabel('Principal components')
plt.legend(loc='best')
plt.show()

# Construct the projection matrix, basically a matrix of our top concatenated vectors
matrix_w = np.hstack((eig_pairs[0][1].reshape(13,1),
                      eig_pairs[1][1].reshape(13,1)))

# Transform our samples into the new projection matrix
Y = X.dot(matrix_w)


# Plot the top two principal components

plt.figure(figsize=(6, 4))
plt.scatter(Y[:,0],Y[:,1])
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 2')
plt.legend(loc='lower center')
plt.show()

# Second Dataset
# Diabetes dataset

diabetes = datasets.load_diabetes()
print diabetes.DESCR

# Scatter two dimensions
plt.figure(figsize=(6, 4))
plt.scatter(diabetes.data[:,0],diabetes.data[:,1])
plt.ylabel('Age')
plt.xlabel('Sex')
plt.show()

X = diabetes.data

# Calculate the means and covariance of the dataset
mean = np.mean(X,axis = 0)
covariance = (X - mean).T.dot((X - mean)) / (X.shape[0] - 1)
print 'Covariance matrix:'
print covariance
print 'Mean:'
print mean

# Calculate the eigenvectors and eigenvalues
eigenValues, eigenVectors = np.linalg.eig(covariance)

print 'EigenValues:'
print eigenValues
print 'EigenVectors'
print eigenVectors

# Make a list of (eigenvalue, eigenvector) tuples
eig_pairs = [(np.abs(eigenValues[i]), eigenVectors[:,i]) for i in range(len(eigenValues))]

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs.sort(key=lambda x: x[0], reverse=True)

# Calculate the explained variance for each component
total = sum(eigenValues)
explained_variance = [(i / total)*100 for i in sorted(eigenValues, reverse=True)]

# Plot the explained variance for each component
plt.figure(figsize=(6, 4))
plt.bar(range(len(explained_variance)), explained_variance, alpha=0.5, align='center',label='Explained variance')
plt.ylabel('Explained variance ratio')
plt.xlabel('Principal components')
plt.legend(loc='best')
plt.show()

# Construct the projection matrix, basically a matrix of our top concatenated vectors
matrix_w = np.hstack((eig_pairs[0][1].reshape(10,1),
                      eig_pairs[1][1].reshape(10,1)))

# Transform our samples into the new projection matrix
Y = X.dot(matrix_w)


# Plot the top two principal components

plt.figure(figsize=(6, 4))
plt.scatter(Y[:,0],Y[:,1])
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 2')
plt.legend(loc='lower center')
plt.show()