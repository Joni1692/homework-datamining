import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import math
from sklearn import datasets
import random
from scipy.interpolate import spline

# Real Function with Noise(to generate targets)
def sinFunctionN(x):
	noise = np.random.normal(0,0.2,1)
	return np.sin(2*math.pi*x) + noise

# Real Function without Noise (to plot the line without noise)
def sinFunction(x):
	return np.sin(2*math.pi*x)

# return an NxM matrix , design matrix
def Phi(x,mu,s):
	num_basis = mu.shape[0]
	phi = array(np.zeros((x.shape[0],num_basis)))
	width = 2.0*(x.max()-x.min())/num_basis
	scale = -1/(2*width*width)
	for i in range(0,num_basis):
		phi[:,i] = np.exp(scale*(x.T-mu[i].T)**2)
	return phi

# create training set of 25 points
x = np.random.uniform(size=(1,25))
x = np.sort(x,axis=1)
x = vstack(x[0])
y = np.array(np.zeros(25))
for i in range(0,25):
	y[i] = sinFunctionN(x[i])
y = vstack(y)
# to plot functions
x1 = np.arange(0.0,1.0,0.01)
y1 = np.array(np.zeros(100))
for i in range(0,100):
	y1[i] = sinFunctionN(x1[i])
y1 = vstack(y1)

# Calculate basis functions, design matrix

num_basis = 10
mu_vector = np.linspace(0,1,num_basis)
mu_vector = vstack(mu_vector)

phi = Phi(x,mu_vector,10)


# plot x,y and basis functions
plt.plot(x, y, 'bo',  x1, sinFunction(x1), 'k')
plt.show() 

# plot the basis functions
for i in range(0,10):
	plt.plot(x,phi[:,i],'r')

plt.show()

# least squares weight estimation
invPhi = np.linalg.inv(phi.T.dot(phi))
maximumWeight = invPhi.dot(phi.T).dot(y)

predictedYLS = maximumWeight.T * phi

for i in range(1,10):
	predictedYLS[:,0] = predictedYLS[:,0] + predictedYLS[:,i]

# plot the least squares weight predictions
plt.plot(x,sinFunction(x),'k',label = 'sin(2pix)')
plt.plot(x,predictedYLS[:,0],'r',label = 'least-squares')

# regularized least squares
lamba = math.exp(2.6)
lamba1 = math.exp(-0.3)
lamba2 = math.exp(-9.4)

invPhi = np.linalg.inv(lamba * np.identity(10) + phi.T.dot(phi))
maximumWeightRS = invPhi.dot(phi.T).dot(y)

predictedRS = maximumWeightRS.T * phi

for i in range(1,10):
	predictedRS[:,0] = predictedRS[:,0] + predictedRS[:,i]

plt.plot(x,predictedRS[:,0],'g',label = 'ln lambda = 2.6')

invPhi = np.linalg.inv(lamba1 * np.identity(10) + phi.T.dot(phi))
maximumWeightRS = invPhi.dot(phi.T).dot(y)

predictedRS = maximumWeightRS.T * phi

for i in range(1,10):
	predictedRS[:,0] = predictedRS[:,0] + predictedRS[:,i]

plt.plot(x,predictedRS[:,0],'b',label = 'ln lambda = -0.3')

invPhi = np.linalg.inv(lamba2 * np.identity(10) + phi.T.dot(phi))
maximumWeightRS = invPhi.dot(phi.T).dot(y)

predictedRS = maximumWeightRS.T * phi

for i in range(1,10):
	predictedRS[:,0] = predictedRS[:,0] + predictedRS[:,i]

plt.plot(x,predictedRS[:,0],'y',label = 'ln lambda = -9.4')

plt.legend(loc = 'upper right')
plt.show()

# predict 100 test data
num_basis1 = 5
mu_vector1 = np.linspace(0,1,num_basis1)
mu_vector1 = vstack(mu_vector1)

lamba3 = math.exp(-5)
phi = Phi(x1,mu_vector1,5)

invPhi = np.linalg.inv(lamba3 * np.identity(5) + phi.T.dot(phi))
maximumWeightRS = invPhi.dot(phi.T).dot(y1)

predictedRS = maximumWeightRS.T * phi

for i in range(1,5):
	predictedRS[:,0] = predictedRS[:,0] + predictedRS[:,i]

MSE = 0

for i in range(0,100):
	MSE = MSE + math.pow((y1[i]-predictedRS[i][0]),2)
MSE = 1.0/100 * MSE

print MSE

plt.plot(x1,predictedRS[:,0],'y',label = 'ln lambda = 4')
plt.plot(x1,y1,'o',label = 'sin(2pix)')
plt.legend(loc = 'upper right')
plt.show()